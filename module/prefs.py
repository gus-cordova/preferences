"""
# prefs.py

Use the default module handling mechanics
to ensure a single instance of preferences
being globally available; any module that
needs access to configuration options
only needs to 'import prefs' and you've
got it.

It's pretty bare bones, if a config item
is not found, you'll get a KeyError; if
an item is expected to be a branch (another
level) and instead it's a leaf item, then
a TypeError is raised.
"""
import re
import json
import datetime

FILENAME = None
DATA = {}

## Lambdas to create a string representation for these types.
TO_STRING = {
        datetime.date: lambda o: '{date:%s}' % o.isoformat(),
        datetime.datetime: lambda o: '{datetime:%s}' % o.isoformat(),
        datetime.time: lambda o: '{time:%s}' % o.isoformat()}

# Individual regexes for date, time and zone
DATE_RE = r'(\d{4})-(\d{2})-(\d{2})'
TIME_RE = r'(\d{2}):(\d{2}):(\d{2})(?:\.(\d+))?'
ZONE_RE = r'([-+]\d\d:?\d\d)?'

# Compiled regexes for date, time, and datetime pattersns
RX_DATE = re.compile(r'\{(date):%s\}' % DATE_RE)
RX_TIME = re.compile(r'\{(time):%s\}' % TIME_RE)
RX_DT = re.compile(r'\{(datetime):%sT%s%s\}' % (DATE_RE, TIME_RE, ZONE_RE))

# Parsers for these types.
def dt2str(obj):
    "Convert the given object to string, or return the object"
    try:
        return TO_STRING[type(obj)](obj)
    except KeyError:
        return obj

def str2dt(value):
    "Convert string representation to datetime object, or return string"
    if isinstance(value, str):
        found = RX_DATE.match(value) or \
                RX_TIME.match(value) or \
                RX_DT.match(value)
        if found:
            got = found.group(1)
            def int_parts(groups=found.groups()[1:]):
                "Parse 'parts' as integers"
                return [int(_ or '0') for _ in groups]
            if got == 'date':
                value = datetime.date(*int_parts())
            elif got == 'time':
                value = datetime.time(*int_parts())
            elif got == 'datetime':
                dtfmt = '%Y-%m-%dT%H:%M:%S.%f'
                if found.groups()[-1]:
                    dtfmt += '%z'
                stamp = value.split(':', 1)[1].split('}', 1)[0]
                value = datetime.datetime.strptime(stamp, dtfmt)
    return value

def get(key):
    "Return a configuration object"
    cfg = DATA
    walked = []
    while '.' in key:
        _, key = key.split('.', 1)
        walked.append(_)
        cfg = cfg[_]
        if isinstance(cfg, dict):
            continue
        raise ValueError("Not a nested level: %s" % '.'.join(walked))
    return str2dt(cfg[key])

def put(key, value):
    "Add an item to the configuration data"
    cfg = DATA
    walked = []
    while '.' in key:
        _, key = key.split('.', 1)
        walked.append(_)
        cfg = cfg.setdefault(_, {})
        if isinstance(cfg, dict):
            continue
        raise ValueError("Not a nested level: %s" % '.'.join(walked))
    cfg[key] = dt2str(value)

def remove(key):
    "Deletes an item from the configuration"
    cfg = DATA
    walked = []
    stack = []
    while '.' in key:
        _, key = key.split('.', 1)
        walked.append(_)
        stack.append(cfg)
        cfg = cfg[_]
        if isinstance(cfg, dict):
            continue
        raise ValueError("Not a nested level: %s" % '.'.join(walked))
    del cfg[key]
    while stack:
        ## Delete any empty branches.
        key = walked.pop()
        cfg = stack.pop()
        if cfg[key]:
            # If this branch is not empty, we're done.
            break
        del cfg[key]

def load(filename=None, exporter=json):
    "Load from the default filename"
    with open(filename or FILENAME) as cfg:
        new_data = exporter.loads(cfg.read())
    DATA.clear()
    DATA.update(new_data)

def save(filename=None, importer=json):
    "Save to the default filename"
    str_data = importer.dumps(DATA)
    with open(filename or FILENAME, 'w') as cfg:
        cfg.write(str_data)

# Fin.
