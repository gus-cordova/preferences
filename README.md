# prefs.py

This is a simple module to handle globally configurable preferences for a python application, be it a script or a service or a desktop app or anything, it tries to keep itself out of the way but render it's (important?) service as unobtrusively as possible.

### Usage:

It's quite simple to use, just `import` the module and use the provided `get`/`put`/`remove` methods to, well, get, put and remove configuration items.

The `load` and `save` methods allow you to load or write the content of the preferences collection to the filesystem. By default, it's written to a `json` file, and the filename is given by the `prefs.FILENAME` module global variable.  You can specify a filename in those methods, and also specify a different exporter/importer module instead of the default `json` that's used.

```python
>>> import prefs
>>> prefs.put('network.credentials.login', 'n3tUs3rN4m3')
>>> prefs.put('network.credentials.password', 'really-hard-password')
>>> import pprint
>>> pprint.pprint(prefs.DATA)
{'network': {'credentials': {'login': 'n3tUs3rN4m3',
                             'password': 'really-hard-password'}}}
>>>
```

As you can see, internally it's kept in the `DATA` global dictionary, and it's stored as nested dictionaries.

```python
>>> import datetime
>>> prefs.put('network.credentials.updated-on', datetime.datetime.now())
>>> pprint.pprint(prefs.DATA)
{'network': {'credentials': {'login': 'n3tUs3rN4m3',
                             'password': 'really-hard-password',
                             'updated-on': '{datetime:2019-11-15T14:18:39.941844}'}}}
>>>
```

It internally converts datetime objects to and from a string representation when the `get` and `put` methods are called, that way it's not necessary to create a special exporter/importer class, this is especially important because the `json` module does not handle datetime objects gracefully. Or at all.

```python
>>> prefs.save('/dev/stdout') # Save to the console, just for kicks.
{"network": {"credentials": {"login": "n3tUs3rN4m3", "password": "really-hard-password", "updated-on": "{datetime:2019-11-15T14:18:39.941844}"}}}
>>> prefs.save('/tmp/pancho.js') # Save to another file, to test save/load
>>> prefs.DATA.clear()
>>> pprint.pprint(prefs.DATA)
{}
>>> prefs.load('/tmp/pancho.js')
>>> pprint.pprint(prefs.DATA)
{'network': {'credentials': {'login': 'n3tUs3rN4m3',
                             'password': 'really-hard-password',
                             'updated-on': '{datetime:2019-11-15T14:18:39.941844}'}}}
>>> prefs.get('network.credentials.updated-on')
datetime.datetime(2019, 11, 15, 14, 18, 39, 941844)
>>> 
```

I've purposefully kept this module as simple as possible, without being _too_ simple so as to not be useful. It can obviously be improved, but this for now serves my needs more than enough.

## Why a module?

The reason why I made it a module is because, one of the major issues I found when trying out other configuration handling options, is that I end up with an object that either has to be made global somehow, or I have to pass around as an argument, and I find that irritating and counter productive too, I've found myself extracting a key and passing that, and then days later figuring out I needed other items from the configuration object that I had to have deeper in the call stack.

Designing this as a module, in fact, probably "abusing" the fact that modules are singletons, circumvented the whole scenario.  Anywhere that I will need access to the application's configuration, `import prefs` brings it into scope; as long as I place the `prefs.load()` call in the right place, by the time any other submodule imports it, all the data I need is already in memory.  Mightily convenient.

That said, enjoy :-)